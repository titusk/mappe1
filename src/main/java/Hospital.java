import java.util.ArrayList;
import java.util.List;
import java.io.*;

/**
 * Class Hospital
 */
public class Hospital {

    /**
     * Variable creation
     */
    private final String hospitalName;

    /**
     * Constructor
     */
    public Hospital (String hospitalName) { this.hospitalName = hospitalName; }

    /**
     * DepartmentRegister Array, stores department data
     */
    ArrayList<Department> departmentRegister = new ArrayList<>();

    /**
     * Getter methods
     */
    public String getHospitalName() { return hospitalName; }

    public List<Department> getDepartments () { return new ArrayList<>(departmentRegister); }

    /**
     * AddDepartment method, adds a new department
     */
    public void addDepartment (Department department) { departmentRegister.add(department); }

    public int getDepartment(String department){
        int i = 0;
        while (i < departmentRegister.size()){
            if (departmentRegister.get(i).getDepartmentName().toLowerCase().equals(department.toLowerCase())){
                return i;
            }
            else {i++;}
        }
        return -1;
    }


    /**
     * toString method
     */
    @Override
    public String toString() { return "Hospital name: " + hospitalName; }
}
