import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import static javax.swing.JOptionPane.*;

/**
 * Class Department
 * @author Titus Kristiansen
 */
public class Department{

    /**
     * Variable creation
     */
    private String departmentName;

    /**
     * Constructor
     */
    public Department(String departmentName) { this.departmentName = departmentName; }

    /**
     * Employees and Patients arrays, they store data about employees and patients respectively
     */
    private ArrayList<Employee> employees = new ArrayList<>();
    private ArrayList<Patient> patients = new ArrayList<>();

    /**
     * Getter method
     */
    public String getDepartmentName() { return departmentName; }

    /**
     * Setter method
     */
    public void setDepartmentName(String departmentName) { this.departmentName = departmentName; }

    /**
     * GetEmployees method, returns a list of employees
     */
    public List<Employee> getEmployees(){ return new ArrayList<>(employees);}

    /**
     * GetPatients method, returns a list of patients
     */
    public List<Patient> getPatients () { return new ArrayList<>(patients); }

    /**
     * AddEmployee method, adds a new patient to the employees array
     */
    public void addEmployee (Employee employee) {employees.add(employee);}

    /**
     * AddPatient method, adds a new patient to the patients array
     */
    public void addPatient (Patient patient) { patients.add(patient);}

    /**
     * equals method
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Department that = (Department) o;
        return departmentName.equals(that.departmentName) &&
                employees.equals(that.employees) &&
                patients.equals(that.patients);
    }

    /**
     * hashCode
     */
    @Override
    public int hashCode() {
        return Objects.hash(departmentName, employees, patients);
    }

    /**
     * Remove method, removes a person from the employee or patient list
     */
    public void remove (Person person) {
        RemoveException removeException = new RemoveException();
        try{patients.remove(person);}catch (Exception e) {showMessageDialog(null,
                removeException);}
        }

    /**
     * toString method
     */
    @Override
    public String toString() {
        return "Department{" +
                "departmentName='" + departmentName + '\'' +
                ", employees=" + employees +
                ", patients=" + patients;
    }
}

/**
 * Class RemoveException
 */
class RemoveException{

    private String message;

    /**
     * Constructor
     */
    public RemoveException() {
        this.message = "Couldn't remove the person, wrong data inserted";
    }

    public String getMessage() {
        return message;
    }
}
