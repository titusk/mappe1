/**
 * Class Employee
 * @author Titus Kristiansen
 */
public class Employee extends Person {

    /**
     * Constructor
     */
    public Employee(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * toString method
     */
    @Override
    public String toString(){return "";}

}

/**
 * Class Nurse
 */
class Nurse extends Employee {

    /**
     * Constructor
     */
    public Nurse(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * toString method
     */
    @Override
    public String toString() {
        return "";
    }
}
