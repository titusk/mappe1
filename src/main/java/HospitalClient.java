import java.util.List;

import static javax.swing.JOptionPane.*;

/**
 * Class HospitalClient
 */
public class HospitalClient {
    public static void main(String[] args) {

        Hospital hospital = new Hospital("St. Olavs Hospital");
        HospitalTestData.fillRegisterWithTestData(hospital);

        while (true) {
            int valg = Integer.parseInt(showInputDialog(

                    "*** HOSPITAL ADMINISTRATION ***\n1. Add a patient\n2. Add an employee\n3. Add a department\n" +
                            "4. List all patients\n5. List all employees\n6. List all departments\n7. Remove a patient"+
                            "\n8. Remove an employee\n9. Diagnose a patient\n10. Quit"));

            switch (valg){
                case 1: {break;}
                case 2: {break;}
                case 3: {break;}
                case 4: {break;}
                case 5: {
                    String list = "";
                    String department = showInputDialog("Department: ");
                    List<Employee> employeeList = hospital.departmentRegister.get(hospital.getDepartment(department)).getEmployees();
                    for (int i = 0; i < employeeList.size(); i++){
                        list += "Patient " + employeeList.get(i).getFullName() + "\n";
                    }
                    showMessageDialog(null, list);
                    break;}
                case 6: {break;}
                case 7: {break;}
                case 8: {}
                case 9: {break;}
                case 10: {System.exit(0);}
                default: showMessageDialog(null, "Type a number from 1 to 10");
            }
        }
    }

        //remove new
        //    exceptions for alle out metodene
        //assertThrows(RemoveException.class, () -> childrenPolyclinic.remove(sondre));
        //arrange act assert
        //illegalargumentexception
}

class HospitalTestData{

    public static void fillRegisterWithTestData(final Hospital hospital){
        Department emergency = new Department ("Emergency");
    emergency.getEmployees().add(new Employee("OddEven","Primtallet",""));
    emergency.getEmployees().add(new Employee("Huppasahn","DelFinito",""));
    emergency.getEmployees().add(new Employee("Rigmor","Mortis",""));
    emergency.getEmployees().add(new GeneralPractitioner("Inco","Gnito",""));
    emergency.getEmployees().add(new Surgeon("Inco","Gnito",""));
    emergency.getEmployees().add(new Nurse("Nina","Teknologi",""));
    emergency.getEmployees().add(new Nurse("Ove","Ralt",""));
    emergency.getPatients().add(new Patient("Inga","Lykke",""));
    emergency.getPatients().add(new Patient("Ulrik","Smål",""));
    hospital.addDepartment(emergency);
    Department childrenPolyclinic = new Department ("Children's Polyclinic");
    childrenPolyclinic.getEmployees().add(new Employee("Salti","Kaffen",""));
    childrenPolyclinic.getEmployees().add(new Employee("NidelV.","Elvefølger",""));
    childrenPolyclinic.getEmployees().add(new Employee("Anton","Nym",""));
    childrenPolyclinic.getEmployees().add(new GeneralPractitioner("Gene","Sis",""));
    childrenPolyclinic.getEmployees().add(new Surgeon("Nanna","Na",""));
    childrenPolyclinic.getEmployees().add(new Nurse("Nora","Toriet",""));
    childrenPolyclinic.getPatients().add(new Patient("Hans","Omvar",""));
    childrenPolyclinic.getPatients().add(new Patient("Laila","La",""));
    childrenPolyclinic.getPatients().add(new Patient("Jøran","Drebli",""));
    hospital.addDepartment(childrenPolyclinic);
    }

    }