/**
 * Abstract Class Person
 * @author Titus Kristiansen
 */
public abstract class Person {

    /**
     * Variable creation
     */
    private String firstName;
    private String lastName;
    private String socialSecurityNumber;

    /**
     * Constructor
     */
    public Person(String firstName, String lastName, String socialSecurityNumber) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.socialSecurityNumber = socialSecurityNumber;
    }

    /**
     * Getter methods
     */
    public String getFirstName() { return firstName; }

    public String getLastName() { return lastName; }

    public String getSocialSecurityNumber() { return socialSecurityNumber; }

    public String getFullName () {return getFirstName() + " " + getLastName();}

    /**
     * Setter methods
     */
    public void setFirstName(String firstName) { this.firstName = firstName; }

    public void setLastName(String lastName) { this.lastName = lastName; }

    public void setSocialSecurityNumber(String socialSecurityNumber) { this.socialSecurityNumber = socialSecurityNumber; }

    /**
     * toString method
     */
    @Override
    public String toString() {
        return "First Name: " + firstName +
                ", Last Name: " + lastName +
                ", Social Security Number: " + socialSecurityNumber;
    }
}

/**
 * Class Patient
 */
class Patient extends Person implements Diagnosable{

    /**
     * Variable creation
     */
    private String diagnosis = "";

    /**
     * Constructor
     */
    public Patient(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * Method SetDiagnosis
     * Sets a patient diagnosis
     */
    public String getDiagnosis() { return diagnosis; }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }
}

/**
 * Interface Diagnosable
 */
interface Diagnosable {
    void setDiagnosis(String diagnosis);
}