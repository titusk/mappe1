/**
 * Abstract class Doctor
 * @author Titus Kristiansen
 */
public abstract class Doctor extends Employee{

    /**
     * Constructor
     */
    public Doctor(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * Abstract method SetDiagnosis
     */
    public abstract void setDiagnosis(Patient patient, String diagnosis);
}

/**
 * Class Surgeon
 */
class Surgeon extends Doctor{

    /**
     * Constructor
     */
    public Surgeon(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * SetDiagnosis method, sets a patient diagnosis
     * @param patient Chooses the correct patient
     * @param diagnosis Sets the diagnosis
     */
    public void setDiagnosis(Patient patient, String diagnosis) {patient.setDiagnosis(diagnosis); }
}

/**
 * Class GeneralPractitioner
 */
class GeneralPractitioner extends Doctor{

    /**
     * Constructor
     */
    public GeneralPractitioner(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * SetDiagnosis method, sets a patient diagnosis
     * @param patient Chooses the correct patient
     * @param diagnosis Sets the diagnosis
     */
    @Override
    public void setDiagnosis(Patient patient, String diagnosis) { patient.setDiagnosis(diagnosis); }
}
